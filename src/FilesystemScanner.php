<?php

namespace z0x\FilesystemScanner;

Class FilesystemScanner
{
    public $files = [];
    private $directories = [];
    public $formats = [];
    private $dir;

    function __construct($directory = null)
    {
        $this->dir = $directory;
    }

    public function set_dir($dir){
        $this->dir = $dir;
        return true;
    }

    private function dir_isset(){
        if(!isset($this->dir)){
            throw new \ErrorException('Directory path must be set in FilesystemScanner');
        }
    }
    public function scan()
    {
        $this->dir_isset();
        array_push($this->directories, $this->dir);
        $this->recurse();
        if ($this->formats !== []){
            $this->files = array_filter($this->files,  [$this,'filter_formats'] );
        }

        return array_values($this->files);
    }

    public function get_all_extensions(){
        $this->dir_isset();
        array_push($this->directories, $this->dir);
        $this->recurse();
        $ext_arr = [];
        foreach ($this->files as $file){
            $arr = explode(".",$file);
            array_push($ext_arr, end($arr));
        }
        $ext_arr = array_unique($ext_arr);
        array_values($ext_arr); //reset the array keys

        return $ext_arr;
    }

    private function get_file_list($dir)
    {
        $files = scandir($dir);
        //echo "Scanning $dir\n";
        foreach ($files as $file) {
            if ($file === "." OR $file === "..") {
                continue;
            }
            if (is_dir($dir . "/" . $file)) {
                array_push($this->directories, $dir . "/" . $file);
            } else {
                array_push($this->files, $dir . "/" . $file);
            }

        }
        return NULL;
    }

    private function recurse()
    {
        if (!empty($this->directories)) {
            foreach ($this->directories as $directory) {
                $this->get_file_list($directory);
                array_shift($this->directories);

            }

            $this->recurse();
        } else {
            //echo "";
        }
    }

    private function filter_formats($file){
        $format = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        if (in_array($format,$this->formats)){
            return true;
        }else return false;
    }
}

